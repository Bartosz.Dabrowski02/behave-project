from behave import given, when, then
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait

from pages.page_factory import Pages
from utils.assertions import assert_number_equal, assert_element_is_present
from utils.base_functions import get_element, get_elements
import re


@given("user is present on the automationpractice home page")
def step_impl(context):
    page = "http://automationpractice.com/"
    context.driver.get(page)


@when("user clicks a {tab_label} tab")
def step_impl(context, tab_label):
    tabs = {
        "WOMAN": Pages.HOME_PAGE.woman_tab,
        "BEST SELLERS": Pages.HOME_PAGE.best_sellers_tab
    }
    tab = get_element(context, tabs[tab_label])
    tab.click()


@then("number of best seller products is equal to {products_number}")
def step_impl(context, products_number):
    list_of_best_sellers = get_elements(context, Pages.HOME_PAGE.number_of_best_sellers_products)
    assert_number_equal(int(products_number), len(list_of_best_sellers))


@when("user selects {value} from 'Sort by' dropdown")
def step_impl(context, value):
    select = Select(get_element(context, Pages.HOME_PAGE.sort_by_dropdown))
    select.select_by_visible_text(value)


@then("user waits until loader is not visible")
def step_impl(context):
    WebDriverWait(context.driver, 30).until(
        EC.invisibility_of_element_located((By.CSS_SELECTOR, "ul.product_list img[src='http://automationpractice.com/img/loader.gif']"))
    )


@when("user searches {search_text}")
def step_impl(context, search_text):
    search_input = get_element(context, Pages.HOME_PAGE.search_input)
    search_input.send_keys(search_text)
    search_button = get_element(context, Pages.HOME_PAGE.search_button)
    search_button.click()


@then("the number of products found is equal to {products_number}")
def step_impl(context, products_number):
    searched_products_list = get_elements(context, Pages.HOME_PAGE.product_list)
    assert_number_equal(int(products_number), len(searched_products_list))
