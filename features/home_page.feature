Feature: Verify the home page

  @1
  Scenario: First test
    Given user is present on the automationpractice home page
    When user clicks a BEST SELLERS tab
    Then number of best seller products is equal to 7

  @2
  Scenario: Second test
    Given user is present on the automationpractice home page
    When user clicks a WOMAN tab
    And user selects Price: Lowest first from 'Sort by' dropdown
    Then user waits until loader is not visible

  @3
  Scenario Outline: Third test
    Given user is present on the automationpractice home page
    When user searches <search_text>
    Then the number of products found is equal to <products_number>

    Examples:
      | search_text | products_number |
      | Dress       | 7               |
      | Printed     | 5               |
      | Faded       | 1               |
