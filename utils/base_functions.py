from selenium.webdriver.common.by import By


def get_by_type(locator_type):
    locators = {
        "class": By.CLASS_NAME,
        "id": By.ID,
        "css": By.CSS_SELECTOR,
        "xpath": By.XPATH,
        "name": By.NAME,
        "link": By.LINK_TEXT,
    }
    return locators.get(locator_type.lower())


def get_element(context, selector):
    locator_type = get_by_type(selector.locator_type)
    return context.driver.find_element(locator_type, selector.locator)


def get_elements(context, selector):
    locator_type = get_by_type(selector.locator_type)
    return context.driver.find_elements(locator_type, selector.locator)
