from selenium.common.exceptions import NoSuchElementException

from utils.base_functions import get_element

DEFAULT_ASSERTION_MESSAGES = {
    "assert_element_is_visible": "We expect that element is visible.",
    "assert_element_is_present": "We expect that element is present.",
    "assert_text_equal": "We expect that '{}' text is equal to '{}'.",
    "assert_number_equal": "We expect that '{}' number is equal to '{}'."
}


def _default_assertion_message(method_name):
    return DEFAULT_ASSERTION_MESSAGES[method_name]


def assertion_message(assertion_name, *args, msg=None):
    if msg == None:
        msg = _default_assertion_message(assertion_name).format(*args)
    raise Exception(msg)


def assert_element_is_present(context, selector, msg=None):
    try:
        get_element(context, selector)
    except NoSuchElementException:
        assertion_message("assert_element_is_present", msg=msg)


def assert_element_is_visible(element, msg=None):
    if not element.is_displayed():
        assertion_message("assert_element_is_visible", msg=msg)


def assert_text_equal(current_text, expected_text, msg=None):
    if current_text != expected_text:
        assertion_message("assert_text_equal", current_text, expected_text, msg=msg)


def assert_number_equal(current_number, expected_number, msg=None):
    if current_number != expected_number:
        assertion_message("assert_number_equal", current_number, expected_number, msg=msg)
