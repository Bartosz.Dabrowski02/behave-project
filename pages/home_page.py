from utils.element import Element


class HomePage:
    best_sellers_tab = Element("css", "a.blockbestsellers")
    number_of_best_sellers_products = Element("css", "ul.blockbestsellers > li")
    woman_tab = Element("css", "a[title='Women']")
    search_input = Element("css", "input#search_query_top")
    search_button = Element("name", "submit_search")
    product_list = Element("css", "ul.product_list > li")

    # todo move it to woman page class
    sort_by_dropdown = Element("css", "#selectProductSort")
    loader = Element("css", "ul.product_list img[src='http://automationpractice.com/img/loader.gif']")

    call_us_span = Element("css", "span.shop-phone")

    input = Element("css", "#get-input input")
    text = Element("css", "#get-input [for=message]")